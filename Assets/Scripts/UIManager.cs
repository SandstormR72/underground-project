﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class UIManager : MonoBehaviour
{
    public NodeNetManager NodeManager;
    public TMP_Dropdown NodeTypeDropdown;
    public Canvas MainCanvas;
    private List<string> DropdownOpt;
    

    public void Start()
    {
        NodeManager = FindObjectOfType<NodeNetManager>();
        DropdownOpt = new List<string>();
        if (NodeTypeDropdown != null) { 
            foreach (GameObject GO in NodeManager.AvailableNodes) 
            {
                DropdownOpt.Add(GO.name);
            }
            NodeTypeDropdown.ClearOptions();
            NodeTypeDropdown.AddOptions(DropdownOpt);
        }
    }
    public void BuildNewNode() 
    {
        NodeManager.AssignBuilding(NodeTypeDropdown.value);
    }
}
