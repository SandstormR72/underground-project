﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrafterNode : Node
{
    public CraftingRecipe Recipe;
    public override IEnumerator ProduceCo()
    {
       if (IsOnline && HasPower) 
        {
            bool HasItemToCraft = true;
            foreach (ItemStack _reqItem in Recipe.RequiredItems) 
            {
               if(!NodeManager.TakeFromStorage(_reqItem)) { HasItemToCraft = false; }
            }
            if (HasItemToCraft) 
            {
                foreach (ItemStack _resItem in Recipe.ResultItems) 
                {
                    NodeManager.AddToStorage(_resItem);
                }
            }
        }
        yield return null;
    }
}
            


