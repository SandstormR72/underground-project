﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProductionNode : Node
{
    public string ProducedItem;
    public int BaseProd, ProdMultip;   
    private GameLib Gamelib;
    private ResorcePatch Patch;
    private bool IsPatchExausted;
    public enum ExtractionState
    {
        Solid,
        Liquid,
        Gas,
        Energy
    };
    public ExtractionState ExtractState;




    public override void Start()
    {
        base.Start();
        IsPatchExausted = false;
        Gamelib = FindObjectOfType<GameLib>();
        GetResouce();
    }

    

    public override IEnumerator ProduceCo()
    {
        if (!IsPatchExausted && Patch != null) 
        {         
            if (IsOnline && HasPower) 
            {                
                NodeManager.AddToStorage(new ItemStack(ProducedItem, BaseProd * ProdMultip));
                Patch.LeftQuantity -= BaseProd;
                if (Patch.LeftQuantity <= 0) { IsOnline = false; IsPatchExausted = true; }
                ProdMultip = Mathf.RoundToInt((Patch.LeftQuantity / Patch.FullQuantity) * Patch.BaseAbundanceMultipl);
                ProdMultip = Mathf.Clamp(ProdMultip, 1, 10);
            }
        }

        yield return null;
    }

    private void GetResouce() 
    {
        RaycastHit hit;        
        if( Physics.Raycast(transform.position, Vector3.down, out hit, 3f)) 
        {
            if (hit.collider.tag != "Terrain"&& hit.collider.GetComponent<ResorcePatch>()!=null) 
            { if (hit.collider.GetComponent<ResorcePatch>().ResourceType.ResourceState.ToString()==ExtractState.ToString())
                { 
                    Patch = hit.collider.GetComponent<ResorcePatch>();
                    ProducedItem = Patch.ResourceType.ResourceName.ToString();
                    ProdMultip = Mathf.RoundToInt(Patch.BaseAbundanceMultipl);
                }
            }
            else { HasPower = false; }
        }
    }
    

}
