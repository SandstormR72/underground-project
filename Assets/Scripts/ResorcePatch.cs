﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResorcePatch : MonoBehaviour
{
    public ResourceType ResourceType;
    public float BaseAbundanceMultipl, LeftQuantity, FullQuantity;
    public Material ThisMat;
    void Awake() 
    {
        ThisMat = GetComponent<MeshRenderer>().material;
        LeftQuantity = FullQuantity;
        ThisMat.SetColor("Col",ResourceType.ResourceColor);
    }    
}
