﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConsumerNode : Node
{
    
   public CraftingRecipe RequiredItems;
    
    public override IEnumerator ProduceCo()
    {
        if (IsOnline && HasPower)
        {
            bool HasRequiredItems = true;
            foreach (ItemStack _reqItem in RequiredItems.RequiredItems)
            {
                if (!NodeManager.TakeFromStorage(_reqItem)) { HasRequiredItems = false; }
            }
            if (HasRequiredItems)
            {
                DoAction();
            }
        }
        yield return null;
    }
    public virtual void DoAction() 
    {
        Debug.Log("Bang!");
    }
}
