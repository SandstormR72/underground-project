﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NodeNetManager : MonoBehaviour
{
    [SerializeField]
    public List<ItemStack> Inventory;
    public int InventorySlots;
    public float Power;
    public float StartPower, PowerUse;
    public bool IsStorageFull;
    public GameObject TubeSection;

    public List<GameObject> AvailableNodes;
    public LayerMask BuildableArea;
    protected bool IsBuilding = false;
    private Vector3 BuildPos;
    protected GameObject SelectedNode;

    void Start()
    {
        Inventory = new List<ItemStack>(InventorySlots);
        for (int i = 0; i < InventorySlots; i++) { Inventory.Add(null); }
        Power = StartPower;
        IsStorageFull = false;
    }


    void Update()
    {
        Power -= PowerUse * Time.deltaTime;
        for (int i = 0; i < Inventory.Count; i++)
        {
            if (Inventory[i] != null)
            {
                if (Inventory[i].Quantity > 100 && Inventory.Count < InventorySlots)
                {
                    Inventory.Add(new ItemStack(Inventory[i].Name, Inventory[i].Quantity - 100));
                    Inventory[i].Quantity = 100;
                }
                if (Inventory.Count > 1)
                {
                    if (Inventory[i].Quantity < 1 || Inventory[i].Name == "") { Inventory.Remove(Inventory[i]); }
                }

            }
        }
        if (Inventory[Inventory.Count - 1] != null)
        {
            if (Inventory.Count == InventorySlots && Inventory[Inventory.Count - 1].Quantity >= 100) { IsStorageFull = true; }
            else { IsStorageFull = false; }
        }

        if (IsBuilding)
        {
            //Send a ray from the mouse cursor to the terrain and see if it hits
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit = new RaycastHit();

            //We hit the terrain, assign the ray hit position
            if (Physics.Raycast(ray, out hit, Mathf.Infinity, BuildableArea))
            {
                BuildPos = new Vector3(hit.point.x, hit.point.y+0.5f, hit.point.z);
            }
            //We didn't hit the terrain.
            else
            {
                BuildPos = Vector3.zero;
            }

            //If the user presses the left mouse button call SpawnBuilding
            if (Input.GetMouseButtonDown(0))
            {
                SpawnBuilding();
            }
            if (Input.GetMouseButtonDown(1))
            {
                IsBuilding=false;
            }
        }
    }



    public void AddToStorage(ItemStack Stack)
    {
        if (!IsStorageFull && Stack != null)
        {
            if (Stack.Quantity > 0 && Stack.Name != "")

            {
                bool StackFound = false;
                for (int i = 0; i < Inventory.Count; i++)
                {
                    if (!StackFound && Inventory[i] != null)
                    {
                        if (Inventory[i].Name == Stack.Name && Inventory[i].Quantity < 100)
                        { Inventory[i].Quantity += Stack.Quantity; StackFound = true; }
                    }
                }
                if (!StackFound) { Inventory.Add(Stack); }
                //Debug.Log("Add Res");
            }
        }

    }
    public bool TakeFromStorage(ItemStack Stack)
    {
        bool StackFound = false;

        for (int i = 0; i < Inventory.Count; i++)
        {
            if (!StackFound && Inventory.Count > 0)
            {
                if (Inventory[i] != null)
                {
                    if (Inventory[i].Name != null && Inventory[i].Name != "")
                    {
                        if (Inventory[i].Name == Stack.Name && Inventory[i].Quantity >= Stack.Quantity)
                        {
                            Inventory[i].Quantity -= Stack.Quantity;
                            StackFound = true;

                        }
                    }
                }
            }
        }
        if (StackFound) { return true; } else { return false; }
    }

    public void PowerUsage(float Pow)
    {
        Power -= Pow;
    }
    public void AssignBuilding(int buildingIndex)
    {
        SelectedNode = AvailableNodes[buildingIndex];
        IsBuilding = true;
    }

    //Spawn the building if the current position is valid.
    public void SpawnBuilding()
    {
        if (BuildPos != Vector3.zero)
        {
            Instantiate(SelectedNode, BuildPos, SelectedNode.transform.rotation);
            IsBuilding = false;
        }

    }
}
    [System.Serializable]
    public class ItemStack
    {
        public string Name;
        public int Quantity;

        public ItemStack(string name, int quantity)
        {
            Name = name;
            Quantity = quantity;
        }
    }
    [System.Serializable]
    public class CraftingRecipe
    {
        public ItemStack[] RequiredItems, ResultItems;
        public CraftingRecipe(ItemStack[] requiredItems, ItemStack[] resultItems)
        {
            RequiredItems = requiredItems;
            ResultItems = resultItems;
        }
    }

