﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameLib : MonoBehaviour
{
    public static GameLib GameLibrary;
    private void Awake()
    {
        GameLibrary = this;
    }
}

[System.Serializable]
public class ResourceType 
{
    public enum ResouceState
    {
        Solid,
        Liquid,
        Gas
    };
    public enum ResouceName
    {
        Coal,
        Bauxite
    };
    public ResouceState ResourceState;
    public ResouceName ResourceName;
    public Color ResourceColor;
}
