﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Node : MonoBehaviour
{
    protected GameLib GameLibrary;
    public bool IsOnline, HasPower;
    public float Timer, TickTime, PowerConsumption, ConnectDistance;
    protected NodeNetManager NodeManager;
    private GameObject TubeSection;
    public List<GameObject> ConnectedObject;
    public virtual void Awake()
    {
        NodeManager = FindObjectOfType<NodeNetManager>();
        GameLibrary = GameLib.GameLibrary;
    }

    public virtual void Start()
    {
        NodeManager = FindObjectOfType<NodeNetManager>();
        TubeSection = NodeManager.TubeSection;
        Timer = 0;
        TryToConnect();
    }
    public void Update()
    {
        Timer += Time.deltaTime;
        if (NodeManager.Power <= 0 && HasPower) { HasPower = false; }
        if (Timer >= TickTime)
        {
            Timer = 0;

            if (HasPower && IsOnline)
            {
                NodeManager.PowerUsage(PowerConsumption);
                StartCoroutine(ProduceCo());
            }
        }
    }
    public virtual IEnumerator ProduceCo()
    {
        yield return null;
    }
    public void OnOffSwitch()
    {
        HasPower = HasPower ? false : true;
    }

    public void TryToConnect()
    {
        Collider[] HitCollider = Physics.OverlapSphere(transform.position, ConnectDistance);
        int connection = 0;
        if (HitCollider.Length > 0)
        {
            foreach (Collider OtherCollider in HitCollider)
            {
                if (OtherCollider != null && OtherCollider.tag != "Terrain")
                {
                    if (OtherCollider.GetComponent<Node>()!=null && OtherCollider.GetComponent<Node>().IsOnline)
                    {
                        List<GameObject> OtherNodeConnections = OtherCollider.GetComponent<Node>().ConnectedObject;
                        if (OtherCollider.transform != this.transform && !OtherNodeConnections.Contains(this.gameObject))
                        {
                            IsOnline = true;
                            HasPower = true;
                            if (OtherCollider.name != "ControlNode") { connection++; }
                            if (connection <= 2)
                            {
                                GenerateTube(this.transform.position, OtherCollider.transform.position);
                            }
                            ConnectedObject.Add(OtherCollider.gameObject);
                        }
                    }
                }

            }

        }
        void GenerateTube(Vector3 PointA, Vector3 PointB)
        {
            float NumberOfSegments = Mathf.RoundToInt(Vector3.Distance(PointA, PointB) / 0.5f);
            float AlonThePath = .25f;
            NumberOfSegments += 1;
            AlonThePath = 1 / (NumberOfSegments);

            for (int i = 1; i < NumberOfSegments; i++)
            {
                Vector3 CreatPosition = PointA + (PointB - PointA) * (AlonThePath * i);
                GameObject tube = Instantiate(TubeSection, CreatPosition, Quaternion.identity);
                tube.transform.position = CreatPosition;
                tube.transform.LookAt(PointB);
                tube.transform.parent = this.transform;
            }




        }
    }
}


